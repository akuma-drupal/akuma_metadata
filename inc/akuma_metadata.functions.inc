<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 */


/**
 *
 */
function akuma_metadata_node_generate($entity, $langcode)
{
    $metatypes = variable_get('akuma_metadata_types', array('jsonld' => 1, 'mocrodata' => 0));
    foreach ($metatypes as $metatype => $status) {
        if ($status) {
            if (function_exists('akuma_metadata_node_generate_' . $metatype)) {
                call_user_func_array('akuma_metadata_node_generate_' . $metatype, array($entity, $langcode));
            }
        }
    }

}

function akuma_metadata_node_generate_jsonld($entity, $langcode, $return = false)
{
    /** @var EntityDrupalWrapper $wrapper */
    $wrapper = entity_metadata_wrapper('node', $entity);
    $body = $wrapper->body->value();
    $body = check_plain($body['safe_value']);
    $info = $wrapper->getPropertyInfo();
    $keywords = (array) explode(',', isset($info['meta_keywords']) ? ($wrapper->meta_keywords->value() ? : '') : '');
    $objDateTime = new DateTime('@' . $wrapper->created->value());
    $isoDate = $objDateTime->format(DateTime::ISO8601);


    $hasImage = false;

    $instances = field_info_instances('node', $entity->type);
    foreach ($instances as $key => $instance) {
        if ('image_image' == $instance['widget']['type']) {
            /** @var array $items */
            $items = field_get_items('node', $entity, $key);
            if ($items && count($items)) {
                foreach ($items as $item) {
                    if (isset($item['uri'])) {
                        $hasImage = file_create_url($item['uri']);
                        break;
                    }
                }
                if ($hasImage) {
                    break;
                }
            }
        }
    }


    $metadata = array(
      "@context"         => "http://schema.org",
      "@type"            => "Article",
      "image"            => $hasImage,
      "headline"         => check_plain($wrapper->title->value()),
      "keywords"         => $keywords,
        /* substitute Drupal field here */
      "datePublished"    => $isoDate,
        /* substitute Drupal field here */
      "articleSection"   => "health",
        /* leave as is */
      "creator"          => "Assuta",
        /* leave as is or substitute field */
      "author"           => "Assuta Israel", //format_username($wrapper->author->value()),
        /* substitute Drupal field here */
      "articleBody"      => $body,
        /* substitute Drupal field - body preview here */
      "mainEntityOfPage" => "True"
        /*   what to put here? */
    );

    if (!$hasImage) {
        unset($metadata['image']);
    }

    if ($return) {
        return $metadata;
    } else {
        $element = array(
          '#theme'      => 'jsonld_tag',
          '#tag'        => 'script',
          '#attributes' => array(
            'type' => 'application/ld+json',
          ),
          '#value'      => json_encode($metadata),
        );
        drupal_add_html_head($element, 'application/ld+json');
    }
}