<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for hooks entity
 */
function akuma_metadata_entity_load($entities, $type)
{
    /**
     * Load Entity Google Schema data
     */
    foreach ($entities as &$entity) {
//        akuma_metadata_entity_gs_data();
    }

}

function akuma_metadata_entity_view($entity, $type, $view_mode, $langcode)
{
    if (($view_mode == 'full')) {
        if (function_exists('akuma_metadata_' . $type . '_generate')) {
            call_user_func_array('akuma_metadata_' . $type . '_generate', array($entity, $langcode));
        }
    }

}

function akuma_metadata_entity_delete($entity, $type)
{

}

function akuma_metadata_entity_insert($entity, $type)
{

}

function akuma_metadata_entity_update($entity, $type)
{

}