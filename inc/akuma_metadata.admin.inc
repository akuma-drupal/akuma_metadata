<?php
/**
 * User  : Nikita.Makarov
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 */


 /**
 * Form builder; Configure settings.
 *
 * @see     system_settings_form()
 */
function akuma_metadata_admin_settings_form($form, $form_state)
{
    $form = array();
    
    $form['#submit'][] = 'akuma_metadata_admin_settings_form_submit';

    return system_settings_form($form);
}

/**
 * @param $form
 * @param $form_state
 */
function akuma_metadata_admin_settings_form_submit($form, &$form_state)
{

}